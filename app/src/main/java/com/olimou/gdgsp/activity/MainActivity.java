package com.olimou.gdgsp.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;

import com.olimou.gdgsp.R;
import com.olimou.gdgsp.fragment.EndFragment;
import com.olimou.gdgsp.fragment.QuestionFragment;
import com.olimou.gdgsp.fragment.RandomFragment;
import com.olimou.gdgsp.helper.FragmentListener;
import com.olimou.gdgsp.helper.Question;
import com.olimou.gdgsp.helper.ViewInfo;

/**
 * Created by Emerson Moura on 24/06/14. (emerson@olimou.com)
 */
public class MainActivity extends ActionBarActivity
		implements FragmentListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		getSupportFragmentManager()
				.beginTransaction().add(R.id.container, new RandomFragment())
				.commit();
	}

	@Override
	public void onEndNumber(int _score) {
		FragmentTransaction lTransaction = getSupportFragmentManager().beginTransaction();
		lTransaction.setCustomAnimations(R.anim.abc_fade_in, R.anim.abc_fade_out);

		lTransaction.add(R.id.container, EndFragment.newInstance(_score), EndFragment.TAG)
				.commit();
	}

	@Override
	public void onQuestionSelected(Question _question, ViewInfo _button) {
		FragmentTransaction lTransaction = getSupportFragmentManager().beginTransaction();
		lTransaction.setCustomAnimations(R.anim.abc_fade_in, R.anim.abc_fade_out);

		lTransaction.add(R.id.container,
				QuestionFragment.newInstance(_question, _button),
				QuestionFragment.TAG)
				.commit();
	}

	@Override
	public void onRetry() {
		Fragment lFragmentByTag = getSupportFragmentManager().findFragmentByTag(EndFragment.TAG);

		if (lFragmentByTag != null) {
			getSupportFragmentManager().beginTransaction()
					.remove(lFragmentByTag)
					.commit();

			getSupportFragmentManager().beginTransaction()
					.replace(R.id.container, new RandomFragment())
					.commit();
		}
	}

	@Override
	public void onSelected() {
		FragmentTransaction lTransaction = getSupportFragmentManager().beginTransaction();
		lTransaction.setCustomAnimations(R.anim.abc_fade_in, R.anim.abc_fade_out);

		Fragment lQuestion = getSupportFragmentManager().findFragmentByTag(QuestionFragment.TAG);

		if (lQuestion != null) {
			lTransaction.remove(lQuestion).commit();
		}
	}
}
