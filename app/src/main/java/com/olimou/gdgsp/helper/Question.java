package com.olimou.gdgsp.helper;

import java.io.Serializable;

/**
 * Created by Emerson Moura on 24/06/14. (emerson@olimou.com)
 */
public class Question implements Serializable {
	private final int mPosition;
	private String mQuestion;
	private Boolean mAnswer;
	private int mRandonPosition;

	public Question(int _position, String _question, String _answer) {
		mQuestion = _question;
		mPosition = _position;

		mAnswer = Boolean.parseBoolean(_answer);
	}

	public int getRandonPosition() {
		return mRandonPosition;
	}

	public void setRandonPosition(int _randonPosition) {
		mRandonPosition = _randonPosition;
	}

	public int getPosition() {
		return mPosition;
	}

	public String getQuestion() {
		return mQuestion;
	}

	public void setQuestion(String _question) {
		mQuestion = _question;
	}

	public Boolean getAnswer() {
		return mAnswer;
	}

	public void setAnswer(Boolean _answer) {
		mAnswer = _answer;
	}
}
