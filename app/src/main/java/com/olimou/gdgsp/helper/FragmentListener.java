package com.olimou.gdgsp.helper;

/**
 * Created by Emerson Moura on 24/06/14. (emerson@olimou.com)
 */
public interface FragmentListener {
	public void onEndNumber(int _score);

	void onRetry();

	public void onSelected();

	public void onQuestionSelected(Question _question, ViewInfo _button);
}
