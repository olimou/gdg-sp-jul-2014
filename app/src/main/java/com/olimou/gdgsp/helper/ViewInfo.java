package com.olimou.gdgsp.helper;

import android.view.View;

import java.io.Serializable;

/**
 * Created by Emerson Moura on 01/07/14. (emerson@olimou.com)
 */
public class ViewInfo implements Serializable {
	private int mLeft;
	private int mTop;
	private int mWidth;
	private int mHeight;

	public ViewInfo(View _view) {
		int[] lPositionXY = new int[2];
		_view.getLocationOnScreen(lPositionXY);

		mLeft = lPositionXY[0];
		mTop = lPositionXY[1];
		mWidth = _view.getWidth();
		mHeight = _view.getHeight();
	}

	public int getLeft() {
		return mLeft;
	}

	public void setLeft(int _left) {
		mLeft = _left;
	}

	public int getTop() {
		return mTop;
	}

	public void setTop(int _top) {
		mTop = _top;
	}

	public int getWidth() {
		return mWidth;
	}

	public void setWidth(int _width) {
		mWidth = _width;
	}

	public int getHeight() {
		return mHeight;
	}

	public void setHeight(int _height) {
		mHeight = _height;
	}
}
