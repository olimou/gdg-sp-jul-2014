package com.olimou.gdgsp.helper;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Emerson Moura on 24/06/14. (emerson@olimou.com)
 */
public class RandomGenerate {
	private final int mListSize;
	private int mIndex = 0;
	private ArrayList<Integer> mNumbersList;

	public RandomGenerate(int _listSize) {
		mListSize = _listSize;
		createRandomList();
	}

	public void reset() {
		createRandomList();
	}

	private void createRandomList() {
		mNumbersList = new ArrayList<Integer>(mListSize);

		for (int i = 0; i < mListSize; i++) {
			mNumbersList.add(i);
		}

		Collections.shuffle(mNumbersList);
	}

	public int getCurrentIndex() {
		return mIndex;
	}

	public int getRandomNumber() throws IndexOutOfBoundsException {
		mIndex++;

		int lSize = mNumbersList.size();

		if (lSize > 0) {
			if (mIndex >= lSize) {
				mIndex = 0;
			}
			return mNumbersList.get(mIndex);
		}

		throw new IndexOutOfBoundsException();
	}

	public boolean removeRandomNumber(int _position) {
		if (_position <= mNumbersList.size()) {
			mNumbersList.remove(_position);
			return true;
		}
		return false;
	}

	public boolean hasNext() {
		return (mNumbersList.size() > 0);
	}

	public ArrayList<Integer> getNumbersList() {
		return mNumbersList;
	}
}

