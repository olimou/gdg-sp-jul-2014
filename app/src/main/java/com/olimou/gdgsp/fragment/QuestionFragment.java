package com.olimou.gdgsp.fragment;

import android.animation.Animator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import com.olimou.customviews.CircleButton;
import com.olimou.gdgsp.R;
import com.olimou.gdgsp.helper.FragmentListener;
import com.olimou.gdgsp.helper.Question;
import com.olimou.gdgsp.helper.ViewInfo;

/**
 * Created by Emerson Moura on 24/06/14. (emerson@olimou.com)
 */
public class QuestionFragment extends Fragment implements View.OnClickListener {

	public static final int ANIMATION_DURACTION = 450;
	public static final String QUESTION_BUTTON = "question_button";
	public static final String QUESTION_INTENT_ACTION = "question_action";
	public static final String QUESTION_PARAMS = "question_param";
	public static final String QUESTION_POSITION = "question_position";
	public static final String RESULT = "question_result";
	public static final String TAG = "question_tag";
	private CircleButton mButtonFalse;
	private ViewInfo mButtonInfo;
	private CircleButton mButtonOK;
	private CircleButton mButtonTrue;
	private FragmentListener mListener;
	private Question mQuestion;

	public QuestionFragment() {
	}

	public static QuestionFragment newInstance(Question _param, ViewInfo _button) {
		QuestionFragment fragment = new QuestionFragment();
		Bundle args = new Bundle();
		args.putSerializable(QUESTION_PARAMS, _param);
		args.putSerializable(QUESTION_BUTTON, _button);
		fragment.setArguments(args);
		return fragment;
	}

	private ViewPropertyAnimator animationButtonIn(AccelerateDecelerateInterpolator _interpolator, CircleButton _buttonFalse, int _duration) {
		setDelta(mButtonInfo, _buttonFalse);
		_buttonFalse.setAlpha(0);
		_buttonFalse.setScaleX(0);
		_buttonFalse.setScaleY(0);

		return _buttonFalse.animate()
				.setInterpolator(_interpolator)
				.setDuration(_duration)
				.alpha(1)
				.scaleX(1)
				.scaleY(1)
				.translationX(0)
				.translationY(0);
	}

	private void animationButtonOut(View _view, int _duration) {
		int[] lScreenLocation = new int[2];

		_view.getLocationOnScreen(lScreenLocation);

		int lLeftDelta = mButtonInfo.getLeft() - lScreenLocation[0];
		int lTopDelta = mButtonInfo.getTop() - lScreenLocation[1];

		_view.animate()
				.alpha(0)
				.scaleX(0)
				.scaleY(0)
				.setDuration(_duration)
				.translationX(lLeftDelta)
				.translationY(lTopDelta);
	}

	private void animationIn() {
		setDelta(mButtonInfo, mButtonOK);

		AccelerateDecelerateInterpolator lInterpolator = new AccelerateDecelerateInterpolator();

		animationButtonIn(lInterpolator, mButtonFalse, ANIMATION_DURACTION);

		animationButtonIn(lInterpolator, mButtonTrue, ANIMATION_DURACTION);

		mButtonOK.animate().setDuration(ANIMATION_DURACTION)
				.setInterpolator(lInterpolator)
				.scaleX(0).scaleY(0)
				.alpha(0);
	}

	private void animationOut() {
		animationButtonOut(mButtonTrue, ANIMATION_DURACTION);

		animationButtonOut(mButtonFalse, ANIMATION_DURACTION);

		mButtonOK.animate()
				.setDuration(ANIMATION_DURACTION)
				.scaleX(1)
				.scaleY(1)
				.alpha(1).setListener(new Animator.AnimatorListener() {
			@Override
			public void onAnimationCancel(Animator animation) {

			}

			@Override
			public void onAnimationEnd(Animator animation) {
				mListener.onSelected();
			}

			@Override
			public void onAnimationRepeat(Animator animation) {

			}

			@Override
			public void onAnimationStart(Animator animation) {

			}
		});
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (FragmentListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onClick(View v) {
		int lId = v.getId();

		boolean lResult = false;

		switch (lId) {
			case R.id.button_true:
				lResult = mQuestion.getAnswer().equals(true);
				break;
			case R.id.button_false:
				lResult = mQuestion.getAnswer().equals(false);
				break;
		}

		if (lResult) {
			Toast lToast = Toast.makeText(getActivity(),
					getActivity().getString(R.string.question_correct),
					Toast.LENGTH_SHORT);
			lToast.setGravity(Gravity.CENTER_VERTICAL | Gravity.BOTTOM, 0, 240);
			TextView lTextView = (TextView) lToast.getView().findViewById(android.R.id.message);
			lToast.getView().setBackgroundColor(getResources().getColor(android.R.color.transparent));
			lTextView.setTextColor(getResources().getColor(R.color.green_accent_A700));
			lToast.show();
		} else {
			Toast lToast = Toast.makeText(getActivity(),
					getActivity().getString(R.string.question_wrong),
					Toast.LENGTH_SHORT);
			lToast.setGravity(Gravity.CENTER_VERTICAL | Gravity.BOTTOM, 0, 240);
			TextView lTextView = (TextView) lToast.getView().findViewById(android.R.id.message);
			lToast.getView().setBackgroundColor(getResources().getColor(android.R.color.transparent));
			lTextView.setTextColor(getResources().getColor(R.color.red_accent_A700));
			lToast.show();
		}

		Intent lIntent = new Intent(QUESTION_INTENT_ACTION);
		lIntent.putExtra(QuestionFragment.QUESTION_POSITION, mQuestion.getRandonPosition());
		lIntent.putExtra(RESULT, lResult);
		getActivity().sendBroadcast(lIntent);

		animationOut();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			mQuestion = (Question) getArguments().getSerializable(QUESTION_PARAMS);
			mButtonInfo = (ViewInfo) getArguments().getSerializable(QUESTION_BUTTON);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		View lInflate = inflater.inflate(R.layout.fragment_question, container, false);

		TextView lQuestionTitle = (TextView) lInflate.findViewById(R.id.text_title);
		TextView lQuestionText = (TextView) lInflate.findViewById(R.id.text_question);

		mButtonTrue = (CircleButton) lInflate.findViewById(R.id.button_true);
		mButtonFalse = (CircleButton) lInflate.findViewById(R.id.button_false);

		mButtonOK = (CircleButton) lInflate.findViewById(R.id.button_ok);
		mButtonOK.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
			@Override
			public boolean onPreDraw() {
				mButtonOK.getViewTreeObserver().removeOnPreDrawListener(this);
				animationIn();
				return true;
			}
		});

		mButtonTrue.setOnClickListener(this);
		mButtonFalse.setOnClickListener(this);

		lQuestionTitle.setText(getActivity().getString(R.string.question) + " " + mQuestion.getPosition());
		lQuestionText.setText(mQuestion.getQuestion());

		return lInflate;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	private void setDelta(ViewInfo _reference, View _view) {
		int lLeftDelta;
		int lTopDelta;

		int[] _screenLocation = new int[2];
		_view.getLocationOnScreen(_screenLocation);

		lLeftDelta = _reference.getLeft() - _screenLocation[0];
		lTopDelta = _reference.getTop() - _screenLocation[1];

		_view.setTranslationY(lTopDelta);
		_view.setTranslationX(lLeftDelta);
	}
}
