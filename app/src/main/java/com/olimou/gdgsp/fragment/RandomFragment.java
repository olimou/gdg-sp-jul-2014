package com.olimou.gdgsp.fragment;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.olimou.customviews.CircleButton;
import com.olimou.gdgsp.R;
import com.olimou.gdgsp.helper.FragmentListener;
import com.olimou.gdgsp.helper.Question;
import com.olimou.gdgsp.helper.RandomGenerate;
import com.olimou.gdgsp.helper.ViewInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Emerson Moura on 24/06/14. (emerson@olimou.com)
 */
public class RandomFragment extends Fragment {

	private BroadcastReceiver mBroadcastReceiver;
	private CircleButton mButton;
	private Handler mHandler;
	private FragmentListener mListener;
	private List<Question> mQuestionsList;
	private RandomGenerate mRandomGenerate;
	private int mScore = 0;
	private TextSwitcher mTextSwitcherNumber;
	private TextSwitcher mTextSwitcherScore;

	public RandomFragment() {
	}

	private void createTextNumber(View _inflate) {
		mTextSwitcherNumber = (TextSwitcher) _inflate.findViewById(R.id.text_number);
		mTextSwitcherNumber.setFactory(new ViewSwitcher.ViewFactory() {
			@Override
			public View makeView() {
				TextView lTextView = new TextView(getActivity());
				lTextView.setTextAppearance(getActivity(), R.style.TextRandomNumber);
				return lTextView;
			}
		});

		Animation lIn = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_count_in);
		Animation lOut = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_count_out);

		mTextSwitcherNumber.setInAnimation(lIn);
		mTextSwitcherNumber.setOutAnimation(lOut);
	}

	private void createTextScore(View _inflate) {
		mTextSwitcherScore = (TextSwitcher) _inflate.findViewById(R.id.text_score);
		mTextSwitcherScore.setFactory(new ViewSwitcher.ViewFactory() {
			@Override
			public View makeView() {
				TextView lTextView = new TextView(getActivity());
				lTextView.setTextAppearance(getActivity(), R.style.TextScore);
				return lTextView;
			}
		});

		Animation lInAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_count_in);
		Animation lOutAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_count_out);

		mTextSwitcherScore.setInAnimation(lInAnimation);
		mTextSwitcherScore.setOutAnimation(lOutAnimation);
	}

	private ArrayList<Question> loadQuestions() {
		String[] lQuestions = getResources().getStringArray(R.array.questions);
		String[] lAnswers = getResources().getStringArray(R.array.answers);

		int lLength = lAnswers.length;

		ArrayList<Question> lQuestionsList = new ArrayList<Question>(lLength);

		if (lQuestions.length == lLength) {
			for (int i = 0; i < lLength; i++) {
				lQuestionsList.add(new Question(i, lQuestions[i], lAnswers[i]));
			}
			return lQuestionsList;
		}
		return null;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (FragmentListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		final View lInflate = inflater.inflate(R.layout.fragment_random, container, false);

		createTextNumber(lInflate);

		createTextScore(lInflate);

		mButton = (CircleButton) lInflate.findViewById(R.id.btn_ok);
		mButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				stopCounter();
				int lCurrentRandomNumber = mRandomGenerate.getRandomNumber();
				int lRandomPosition = mRandomGenerate.getCurrentIndex();
				Question lQuestion = mQuestionsList.get(lCurrentRandomNumber);
				lQuestion.setRandonPosition(lRandomPosition);

				ViewInfo lButtonInfo = new ViewInfo(mButton);
				mListener.onQuestionSelected(lQuestion, lButtonInfo);
			}
		});

		mQuestionsList = loadQuestions();

		mRandomGenerate = new RandomGenerate(mQuestionsList.size());

		mHandler = new Handler(new Handler.Callback() {
			@Override
			public boolean handleMessage(Message msg) {
				if (mRandomGenerate.hasNext()) {
					int lRandomNumber = mRandomGenerate.getRandomNumber();
					String lNumber = Integer.toString(lRandomNumber);
					mTextSwitcherNumber.setText(lNumber);
					mHandler.sendEmptyMessageDelayed(0, getResources().getInteger(R.integer.animation_duration) * 1);
				} else {
					mButton.setEnabled(false);
					mListener.onEndNumber(mScore);
				}
				return true;
			}
		});

		mBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context _context, Intent _intent) {
				onReceivedBroadcast(_context, _intent);
			}
		};

		setScore();

		return lInflate;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	@Override
	public void onPause() {
		super.onPause();
		getActivity().unregisterReceiver(mBroadcastReceiver);
		stopCounter();
	}

	public void onReceivedBroadcast(Context _context, Intent _intent) {
		if (_intent != null) {
			boolean lResult = _intent.getBooleanExtra(QuestionFragment.RESULT, false);

			if (lResult) {
				mScore++;
				setScore();
			} else {

			}
			int lIntExtra = _intent.getIntExtra(QuestionFragment.QUESTION_POSITION, 0);
			mRandomGenerate.removeRandomNumber(lIntExtra);
		}

		startCounter();
	}

	@Override
	public void onResume() {
		super.onResume();
		getActivity().registerReceiver(
				mBroadcastReceiver,
				new IntentFilter(QuestionFragment.QUESTION_INTENT_ACTION));
		startCounter();
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	private void setScore() {
		mTextSwitcherScore.setText(getActivity().getString(R.string.total_score)
				+ " " + Integer.toString(mScore));
	}

	private void startCounter() {
		mHandler.sendEmptyMessage(0);
	}

	private void stopCounter() {
		mHandler.removeCallbacksAndMessages(null);
	}
}
