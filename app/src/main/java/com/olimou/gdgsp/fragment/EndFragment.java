package com.olimou.gdgsp.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.olimou.customviews.CircleButton;
import com.olimou.gdgsp.R;
import com.olimou.gdgsp.helper.FragmentListener;

/**
 * Created by Emerson Moura on 01/07/14. (emerson@olimou.com)
 */
public class EndFragment extends Fragment {
	private static final String SCORE = "score";
	public static final String TAG = "endfragment";
	private FragmentListener mListener;
	private int mScore;

	public EndFragment() {
	}

	public static EndFragment newInstance(int _param) {
		EndFragment fragment = new EndFragment();
		Bundle args = new Bundle();
		args.putInt(SCORE, _param);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (FragmentListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			mScore = getArguments().getInt(SCORE);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		View lView = inflater.inflate(R.layout.fragment_end, container, false);

		TextView lTextView = (TextView) lView.findViewById(R.id.score);
		lTextView.setText(getActivity().getString(R.string.total_score) + Integer.toString(mScore));

		CircleButton lButton = (CircleButton) lView.findViewById(R.id.btn_retry);
		lButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mListener.onRetry();
			}
		});

		return lView;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}
}
