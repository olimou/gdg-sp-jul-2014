package test.com.olimou.gdgsp.test.helper;

import android.test.InstrumentationTestCase;

import com.olimou.gdgsp.helper.RandomGenerate;

import java.util.ArrayList;

/**
 * Created by Emerson Moura on 24/06/14. (emerson@olimou.com)
 */
public class RandomGenerateTest extends InstrumentationTestCase {

    public void test() {
        int lQuestionsNumber = 10;

        RandomGenerate lRandomGenerate = new RandomGenerate(lQuestionsNumber);

        ArrayList<Integer> lIntegerArray = new ArrayList<Integer>(lQuestionsNumber);

        while (lRandomGenerate.hasNext()) {
            int lRandomNumber = lRandomGenerate.getRandomNumber();
            lRandomGenerate.getRandomNumber();
            assertEquals(lRandomGenerate.getNumbersList().contains(lRandomNumber), false);
        }
    }
}
