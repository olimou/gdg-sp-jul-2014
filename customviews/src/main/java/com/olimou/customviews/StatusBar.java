package com.olimou.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Emerson Moura on 24/06/14. (emerson@olimou.com)
 */
public class StatusBar extends View {
	private int mColor = Color.RED;
	private int mStatusBarHeight;

	public StatusBar(Context context) {
		super(context);
		init(null, 0);
	}

	public StatusBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs, 0);
	}

	public StatusBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs, defStyle);
	}

	public int getActionBarHeight() {
		int lResourceId = (int) (getResources().getDimension(R.dimen.abc_action_bar_default_height));

		return lResourceId;
	}

	public int getStatusBarHeight() {
		int result = 0;

		int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");

		if (resourceId > 0) {
			result = getResources().getDimensionPixelSize(resourceId);
		}

		return result;
	}

	private void init(AttributeSet attrs, int defStyle) {
		final TypedArray a = getContext().obtainStyledAttributes(
				attrs, R.styleable.StatusBar, defStyle, 0);

		mColor = a.getColor(
				R.styleable.StatusBar_color,
				mColor);

		mStatusBarHeight = 0;

		mStatusBarHeight = getStatusBarHeight() + getActionBarHeight();

		a.recycle();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		if (Build.VERSION.SDK_INT >= 19) {
			Paint lPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
			lPaint.setColor(mColor);
			canvas.drawRect(0, 0, getWidth(), mStatusBarHeight, lPaint);
		}
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);

		setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), mStatusBarHeight);
	}
}
