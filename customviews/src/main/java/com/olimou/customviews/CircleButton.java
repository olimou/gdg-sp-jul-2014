package com.olimou.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.View;

/**
 * Created by Emerson Moura on 24/06/14. (emerson@olimou.com)
 */
public class CircleButton extends View implements View.OnClickListener, View.OnTouchListener {
	public static final double ALPHA_DEFAULT = 0.9;
	private int mBackground = Color.DKGRAY;
	private float mDimension = 24;
	private Drawable mDrawable;
	private int mShadowSize = 0;
	private boolean mShowShadow;
	private String mText = "";
	private int mTextColor = Color.WHITE;
	private float mTextHeight;
	private TextPaint mTextPaint;
	private float mTextWidth;

	public CircleButton(Context context) {
		super(context);
		init(null, 0);
	}

	public CircleButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs, 0);
	}

	public CircleButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs, defStyle);
	}

	private int getColor(int _background, float _value) {
		float[] lHSV = new float[3];
		Color.RGBToHSV(Color.red(_background), Color.green(_background), Color.blue(_background), lHSV);

		lHSV[2] = lHSV[2] + _value;

		return Color.HSVToColor(lHSV);
	}

	private void init(AttributeSet attrs, int defStyle) {
		setSoundEffectsEnabled(true);

		final TypedArray a = getContext().obtainStyledAttributes(
				attrs, R.styleable.CircleButton, defStyle, 0);

		mText = a.getString(R.styleable.CircleButton_text);

		mBackground = a.getColor(R.styleable.CircleButton_circle_background, mBackground);

		mTextColor = a.getColor(R.styleable.CircleButton_textColor, mTextColor);

		if (a.hasValue(R.styleable.CircleButton_drawable)) {
			mDrawable = a.getDrawable(R.styleable.CircleButton_drawable);
			mDrawable.setCallback(this);
		}

		mDimension = a.getDimension(
				R.styleable.CircleButton_textSize,
				mDimension);

		mShowShadow = a.getBoolean(R.styleable.CircleButton_showShadow, true);

		mShadowSize = a.getInt(R.styleable.CircleButton_shadowSize, 0);

		mTextPaint = new TextPaint();
		mTextPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
		mTextPaint.setTextAlign(Paint.Align.LEFT);

		a.recycle();

		invalidateTextPaintAndMeasurements();

		setOnClickListener(this);
		setOnTouchListener(this);
	}

	private void invalidateTextPaintAndMeasurements() {
		if (mText == null) {
			mText = "";
		}

		mTextPaint.setTextSize(mDimension);
		mTextPaint.setColor(mTextColor);
		mTextWidth = mTextPaint.measureText(mText);

		Paint.FontMetrics fontMetrics = mTextPaint.getFontMetrics();
		mTextHeight = fontMetrics.bottom;
	}

	@Override
	public void onClick(View v) {
		playSoundEffect(SoundEffectConstants.CLICK);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		int paddingLeft = getPaddingLeft();
		int paddingTop = getPaddingTop();
		int paddingRight = getPaddingRight();
		int paddingBottom = getPaddingBottom();

		int contentWidth = getWidth() - paddingLeft - paddingRight;
		int contentHeight = getHeight() - paddingTop - paddingBottom;

		float lCircleSize = Math.min(contentHeight, contentWidth);


		int lColorGradientStart = getColor(mBackground, (float) 0.15);
		int lColorGradientEnd = getColor(mBackground, (float) -0.1);

		Paint lPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

		float lShadowRadius = 0;

		if (mShowShadow) {
			float lShadow;

			if (mShadowSize == 0) {
				lShadow = (float) 0.06;
			} else {
				if (mShadowSize > 100) {
					mShadowSize = 100;
				}
				lShadow = (float) mShadowSize / 100;
			}

			lShadowRadius = (lCircleSize * lShadow);

			lCircleSize = lCircleSize - (lShadowRadius * 2);

			lPaint.setShadowLayer(lShadowRadius, 0, 0, Color.DKGRAY);
			setLayerType(LAYER_TYPE_SOFTWARE, lPaint);
		}

		lPaint.setColor(mBackground);
		lPaint.setStyle(Paint.Style.FILL);

		canvas.drawCircle(
				contentWidth / 2 + paddingLeft,
				contentHeight / 2 + paddingTop,
				(lCircleSize / 2),
				lPaint);

		lPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		lPaint.setShader(new LinearGradient(0, 0, 0, lCircleSize, lColorGradientStart, lColorGradientEnd, Shader.TileMode.MIRROR));

		canvas.drawCircle(
				contentWidth / 2 + paddingLeft,
				contentHeight / 2 + paddingTop,
				(lCircleSize / 2),
				lPaint);

		lPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		lPaint.setColor(mBackground);

		canvas.drawCircle(
				contentWidth / 2 + paddingLeft,
				contentHeight / 2 + paddingTop,
				(float) ((lCircleSize / 2) - lCircleSize * 0.02),
				lPaint);

		canvas.drawText(mText,
				paddingLeft + (contentWidth - mTextWidth) / 2,
				paddingTop + (contentHeight) / 2 + mTextHeight,
				mTextPaint);

		if (mDrawable != null) {
			int lSize = (int) (lCircleSize * .6);

			int lLeft = (int) ((contentWidth - lSize) / 2);
			int lTop = (int) ((contentHeight - lSize) / 2);
			int lRight = (int) (lLeft + lSize);
			int lBottom = (int) (lTop + lSize);

			mDrawable.setBounds(lLeft, lTop,
					lRight, lBottom);
			mDrawable.draw(canvas);
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		int lAction = event.getAction();

		switch (lAction) {
			case MotionEvent.ACTION_DOWN:
				setAlpha((float) ALPHA_DEFAULT);
				return false;
			case MotionEvent.ACTION_UP:
				setAlpha((float) 1);
				return false;
		}
		return false;
	}
}
